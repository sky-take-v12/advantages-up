package com.itheima.advantagesup.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * JwtProperties
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 16:51
 */
@Data
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {

    private String secretKey;
    private Long ttl;
    private String header;
}
