package com.itheima.advantagesup.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * R
 *
 * @author liliudong
 * @version 1.0
 * @description 全局统一响应类
 * @date 2023/7/26 15:56
 */
@AllArgsConstructor
@Getter
public class R<T> {
    private final static String SUCCESS = "success";

    private int errno;
    private String errmsg;
    private T data;


    /**
     * 成功
     *
     * @param data   数据
     * @param errmsg errmsg
     * @param errno  errno
     * @return {@link R}<{@link T}>
     */
    public static <T> R<T> success(T data, String errmsg, int errno) {
        return new R<>(errno, errmsg, data);
    }

    /**
     * 成功
     *
     * @param data   数据
     * @param errmsg errmsg
     * @return {@link R}<{@link T}>
     */
    public static <T> R<T> success(T data, String errmsg) {
        return success(data, errmsg, 0);
    }

    /**
     * 成功
     *
     * @param data 数据
     * @return {@link R}<{@link T}>
     */
    public static <T> R<T> success(T data) {
        return success(data, SUCCESS);
    }

    /**
     * 成功
     *
     * @return {@link R}<{@link ?}>
     */
    public static R<?> success() {
        return success(null);
    }

    public static R<?> error(int code, String errmsg) {
        return new R<>(code, errmsg, null);
    }

    /**
     * 错误
     *
     * @param code 代码
     * @return {@link R}<{@link ?}>
     */
    public static R<?> error(int code) {
        return error(code, "error");
    }

    /**
     * 错误
     *
     * @param errmsg errmsg
     * @return {@link R}<{@link ?}>
     */
    public static R<?> error(String errmsg) {
        return error(1000, errmsg);
    }

    /**
     * 错误
     *
     * @return {@link R}<{@link ?}>
     */
    public static R<?> error() {
        return error(1000);
    }
}
