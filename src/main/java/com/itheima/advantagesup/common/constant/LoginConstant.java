package com.itheima.advantagesup.common.constant;

/**
 * LoginConstant
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:11
 */
public interface LoginConstant {
    String USER_ID = "userId";

}
