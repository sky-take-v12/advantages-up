package com.itheima.advantagesup.config;

import com.itheima.advantagesup.properties.JwtProperties;
import com.itheima.advantagesup.utils.JwtUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JwtConfiguration
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 16:52
 */
@Configuration
@EnableConfigurationProperties(JwtProperties.class)
public class JwtConfiguration {

    @Bean
    public JwtUtil jwtUtil(JwtProperties jwtProperties) {
        return new JwtUtil(jwtProperties.getSecretKey(), jwtProperties.getTtl());
    }
}
