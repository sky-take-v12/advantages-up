package com.itheima.advantagesup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvantagesUpApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvantagesUpApplication.class, args);
    }

}
