package com.itheima.advantagesup.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class MenuVO {
    private Integer id;
    private Integer pid;
    private String path;
    private String component;
    private String title;
    private String icon;
    private Integer is_show;
    private Integer is_cache;
    private Integer is_link;
    private String redirect;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Integer del_flag;
    private Integer type;
    private BigDecimal sort;
    private String mark;
}
