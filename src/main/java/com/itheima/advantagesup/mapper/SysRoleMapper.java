package com.itheima.advantagesup.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SysRoleMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:51
 */
@Mapper
public interface SysRoleMapper {
    /**
     * 选择名单由ids
     *
     * @param roleIds 角色id
     * @return {@link List}<{@link String}>
     */
    List<String> selectNameListByIds(List<String> roleIds);
}
