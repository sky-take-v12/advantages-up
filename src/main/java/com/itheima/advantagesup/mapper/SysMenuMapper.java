package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SysMenuMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:57
 */
@Mapper
public interface SysMenuMapper {
    /**
     * 选择列表按角色id
     *
     * @param roleIds 角色ids
     * @return {@link List}<{@link SysMenu}>
     */
    List<SysMenu> selectListByRoleIds(List<String> roleIds);
}
